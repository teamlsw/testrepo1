﻿using System;
using Microsoft.VisualBasic;

namespace WatersFinalQ6
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double amount, term, rate;

            amount = Int32.Parse(args[0]);
            term = Int32.Parse(args[1]) * 12;
            rate = Double.Parse(args[2]) / 100 / 12;

            double pymt = Math.Round(Financial.Pmt(rate, term, -amount), 2);

            Console.WriteLine(pymt);

        }
    }
}