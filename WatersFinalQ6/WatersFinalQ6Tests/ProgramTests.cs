﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System;

namespace WatersFinalQ6.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void MainTest()
        {
            Process proc = new Process();

            proc.StartInfo.FileName = @".\..\..\..\\WatersFinalQ6\bin\Debug\WatersFinalQ6.exe";

            proc.StartInfo.Arguments = @"100000 10 5";

            proc.StartInfo.RedirectStandardOutput = true;

            proc.StartInfo.UseShellExecute = false;

            proc.Start();

            proc.WaitForExit();

            string pymt = proc.StandardOutput.ReadLine();

            string expOutput = "1060.66";

            Assert.AreEqual(pymt, expOutput);
        }
    }
}