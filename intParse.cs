public static int Parse(string s)
        {
            if (s == null) throw new ArgumentNullException();
            if (s == string.Empty) throw new ArgumentNullException();

            int result = 0,
                mult = 1,
                start = 0,
                negative = 1,
                digit;

            if (s[0] == '-')
            {
                start = 1;
                negative = -1;
            }
            else if (s[0] == '+')
            {
                start = 1;
            }

            for (int i = s.Length - 1; i >= start; i--)
            {
                if (s[i] < 48 || s[i] > 57)
                {
                    throw new ArgumentOutOfRangeException();
                }

                digit = s[i] - 48;

                if (mult >= 1000000000 && (digit > 2 || (digit == 2 &&
                    (result > 147483648 || (result > 147483647 && negative == 1)))))
                {
                    throw new ArgumentOutOfRangeException();
                }

                result += digit * mult;
                mult *= 10;
            }

            result *= negative;
            return result;
        }