 public class UnitTest1
    {
        private DateTime testDate;

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParseTestWithNull()
        {
            Program.parse(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithZeroYear()
        {
            Program.parse("01/30/0000");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithZeroMonth()
        {
            Program.parse("00/30/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithZeroDay()
        {
            Program.parse("01/00/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithJan32()
        {
            Program.parse("01/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithFeb30()
        {
            Program.parse("02/30/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithMar32()
        {
            Program.parse("03/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithApr31()
        {
            Program.parse("04/31/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithMay32()
        {
            Program.parse("05/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithJun31()
        {
            Program.parse("06/31/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithJul32()
        {
            Program.parse("07/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithAug32()
        {
            Program.parse("08/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithSep31()
        {
            Program.parse("09/31/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithOct32()
        {
            Program.parse("10/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithNov32()
        {
            Program.parse("11/31/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithDec32()
        {
            Program.parse("12/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void ParseTestWithMonth13()
        {
            Program.parse("13/01/2015");
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void ParseTest12()
        {
            Program.parse("12/31/10000");

        }
        [TestMethod()]
        public void ParseTest13()
        {

            Assert.AreEqual(Program.parse("12/31/9998"), DateTime.Parse("12/31/9998"));
        }
        [TestMethod()]
        public void ParseTest14()
        {
            // DateTime result = Program.parse("12/31/2015");
            Assert.AreEqual(Program.parse("12/31/2015"), DateTime.Parse("12 / 31 / 2015"));
        }
        [TestMethod()]
        public void ParseTest15()
        {
            Assert.AreEqual(Program.parse("11/30/2015"), DateTime.Parse("11 / 30 / 2015"));
        }
        [TestMethod()]
        public void ParseTest16()
        {
            Assert.AreEqual(Program.parse("10/31/2015"), DateTime.Parse("10/31/2015"));
        }
        [TestMethod()]
        public void ParseTest17()
        {
            Assert.AreEqual(Program.parse("09/30/2015"), DateTime.Parse("09/30/2015"));
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void parseTest()
        {
            Program.parse("1//00000");
        }
        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void parseTest1()
        {
            Program.parse("1/1/0000");
        }
        [TestMethod()]
        public void parseTest2()
        {
            Assert.AreEqual(Program.parse("01/01/2015"), DateTime.Parse("01/01/2015"));
        }
        [TestMethod()]
        public void parseTest3()
        {
            Assert.AreEqual(Program.parse("01/31/2015"), (testDate = new DateTime(2015, 1, 31)));
        }
        [TestMethod()]
        public void parseTest4()
        {
            Assert.AreEqual(Program.parse("02/28/2015"), (testDate = new DateTime(2015, 2, 28)));
        }
        [TestMethod()]
        public void parseTest5()
        {
            Assert.AreEqual(Program.parse("03/31/2015"), (testDate = new DateTime(2015, 3, 31)));
        }
        [TestMethod()]
        public void parseTest6()
        {
            Assert.AreEqual(Program.parse("04/30/2015"), (testDate = new DateTime(2015, 4, 30)));
        }
        [TestMethod()]
        public void parseTest7()
        {
            Assert.AreEqual(Program.parse("05/31/2015"), (testDate = new DateTime(2015, 5, 31)));
        }
        [TestMethod()]
        public void parseTest8()
        {
            Assert.AreEqual(Program.parse("06/30/2015"), (testDate = new DateTime(2015, 6, 30)));
        }
        [TestMethod()]
        public void parseTest9()
        {
            Assert.AreEqual(Program.parse("07/31/2015"), (testDate = new DateTime(2015, 7, 31)));
        }
        [TestMethod()]
        public void parseTest10()
        {
            Assert.AreEqual(Program.parse("08/31/2015"), (testDate = new DateTime(2015, 8, 31)));
        }
        [TestMethod()]
        public void parseTest11()
        {
            Assert.AreEqual(Program.parse("09/30/2015"), (testDate = new DateTime(2015, 9, 30)));
        }
    }