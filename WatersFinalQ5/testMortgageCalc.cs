<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head profile="http://selenium-ide.openqa.org/profiles/test-case">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="selenium.base" href="http://www.bankrate.com/" />
<title>testMortgageCalc</title>
</head>
<body>
<table cellpadding="1" cellspacing="1" border="1">
<thead>
<tr><td rowspan="1" colspan="3">testMortgageCalc</td></tr>
</thead><tbody>
<tr>
	<td>open</td>
	<td>/partners/sem/mortgage-calculator-rates.aspx</td>
	<td></td>
</tr>
<tr>
	<td>type</td>
	<td>id=loanAmount</td>
	<td>100000</td>
</tr>
<tr>
	<td>type</td>
	<td>id=years</td>
	<td>10</td>
</tr>
<tr>
	<td>type</td>
	<td>id=interestRate</td>
	<td>5</td>
</tr>
<tr>
	<td>clickAndWait</td>
	<td>id=calcButton</td>
	<td></td>
</tr>
<tr>
	<td>assertText</td>
	<td>id=mpay</td>
	<td>$1,060.66</td>
</tr>

</tbody></table>
</body>
</html>
