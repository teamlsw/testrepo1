﻿using Microsoft.VisualBasic;
using System;
using System.Diagnostics;
using System.Threading;

namespace WatersFinalQ7
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Thread th = new Thread(CalcMortgage);

            th.Start();
        }

        private static void CalcMortgage()
        {
            Stopwatch sw = new Stopwatch();

            double amount, term, rate, avg = 0;
            int loopCount = 5;

            amount = 100000;
            term = 10 * 12;
            rate = 0.05 / 12;

            for (int i = 0; i < loopCount; i++)
            {
                sw.Reset();
                sw.Start();
                Thread.Sleep(i * 1000);
                double pymt = Math.Round(Financial.Pmt(rate, term, -amount), 2);
                sw.Stop();
                Console.WriteLine("Elapsed time: " + sw.ElapsedMilliseconds + "ms,  Payment: $" + pymt);
                Console.WriteLine();
                avg += sw.ElapsedMilliseconds;
            }
            avg = avg / loopCount;
            Console.WriteLine();
            Console.WriteLine("Average time: " + avg + "ms");
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}