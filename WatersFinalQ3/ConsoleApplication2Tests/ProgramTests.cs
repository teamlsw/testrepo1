﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace ConsoleApplication2.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod()]
        public void MainTest()
        {
            double amount, term, rate;
            
            amount = 100000;
            term = 10*12;
            rate = 0.05 / 12;

            double pymt = Math.Round(Financial.Pmt(rate, term, amount * -1),2);

            Assert.AreEqual(1060.66, pymt);
        }
    }
}