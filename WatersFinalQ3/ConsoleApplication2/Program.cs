﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace ConsoleApplication2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double amount, term, rate;

            amount = 100000;
            term = 10 * 12;
            rate = 0.05 / 12;

            double pymt = Math.Round(Financial.Pmt(rate, term, amount*-1), 2);

            Console.WriteLine("Monthly payments will be: " + pymt);
            
            Console.ReadLine();
        }
    }
}
